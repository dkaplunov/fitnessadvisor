'use strict';

const assert = require("assert");
const sinon = require("sinon");
const userService = require ('../src/services/users-service');
const bcrypt = require('bcrypt');
const messages = require('../src/resources/constants').constants;

describe("User Service", () => {
    const userInfo = {name:"user1",password:"12121",isTrainer:false};
    const userAdapter = {findUserByName: ()=>{}, createUser: ()=>{}, getTrainerInfo: ()=>{}, updateTrainerInfo: ()=>{}};
    const userExistAdapter = {...userAdapter, findUserByName: ()=>{return {name:"user1",password:"12121"}}, updateUser: ()=>{}};
    const resultObject = new Promise((resolve, reject) =>
        resolve({
            name:userInfo.name,
            password:bcrypt.hashSync(userInfo.password, 10),
            isTrainer:false
        })
    );

    it("Test logging in", (done) => {
        const findUserByNameStub = sinon.stub(userAdapter, "findUserByName").returns(resultObject);
        sinon.stub(userAdapter, "getTrainerInfo").returns(resultObject);
        userService.signIn({name:"user1",password:"12121"}, userAdapter).
        then((result)=>{assert.equal(result.mess, messages.SUCCESS_AUTHENTICATION);done();}).
        catch(()=>{assert.fail();done(messages.TEST_FAILED)});
    });

    it("Test add user", (done) => {
        const saveUserStub = sinon.stub(userAdapter, "createUser").returns(resultObject);
        sinon.stub(userAdapter, "updateTrainerInfo").returns(resultObject);
        userService.signUp({name:"user1",password:"12121"}, userAdapter).
        then((result)=>{assert.equal(result.message, messages.SUCCESS_SAVED);done()}).
        catch(()=>{assert.fail();done(messages.TEST_FAILED)});
    });

    it("Test update user", (done) => {
        const saveUserStub = sinon.stub(userExistAdapter, "updateUser").returns(resultObject);
        sinon.stub(userExistAdapter, "updateTrainerInfo").returns(resultObject);
        userService.update({name:"user1",password:"12121", isTrainer:false}, userExistAdapter).
        then((result)=>{assert.equal(result.message, messages.SUCCESS_SAVED);done()}).
        catch(()=>{assert.fail();done(messages.TEST_FAILED)});
    });

});