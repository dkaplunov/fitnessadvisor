FROM node:10

# Create app directory
WORKDIR /app

COPY package*.json ./

RUN yarn install

# Bundle app source
COPY ./src ./src
COPY ./database.json ./database.json
COPY ./api-doc ./api-doc
COPY ./migrations ./migrations
COPY ./tests ./tests


EXPOSE 3000
CMD [ "yarn", "start" ]