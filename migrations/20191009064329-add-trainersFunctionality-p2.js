'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  db.addColumn('users', 'height', {type:'int'}, errFunction);
  db.addColumn('users', 'weight', {type:'int'}, errFunction);
  db.addColumn('users', 'age', {type:'int'}, errFunction);
  db.addColumn('users', 'first_name', {type:'string'}, errFunction);
  db.addColumn('users', 'last_name', {type:'string'}, errFunction);


  db.runSql(
        " ALTER TABLE public.sport_kinds ALTER COLUMN id SET DEFAULT nextval('workouts_seq'::regclass);",
        [], errFunction);

    return db.createTable('trainers_sport_kinds', {
      trainer_id: { type: 'bigint',
        primaryKey: true,
        foreignKey: {
          name: 'trainers_id_fk',
          table: 'trainers',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'user'
        }
      },
      kind_id: { type: 'bigint',
        primaryKey: true,
        foreignKey: {
          name: 'kind_sport_id_fk',
          table: 'sport_kinds',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
    });
};

exports.down = function(db) {
  return null;
};

const errFunction = err=>console.info(`Error column adding: ${err}`);

exports._meta = {
  "version": 1
};
