'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createTable('workouts', {
    id: {
      type: 'bigint',
      primaryKey: true
    },
    date: {type:'date'},
    description: {type:'string', length: 200},
    user: {
      type: 'bigint',
      foreignKey: {
        name: 'users_id_fk',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    }
  });

  db.createTable('workouts_tasks', {
    id: { type: 'bigint',
      primaryKey: true
    },
    date: {type:'date'},
    description: {type:'string', length: 200},
    amount: {type:'int'},
    workout: {
      type: 'bigint',
      foreignKey: {
        name: 'workout_id_fk',
        table: 'workouts',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    },
    taskId: {
      type: 'bigint',
      foreignKey: {
        name: 'tasks_id_fk',
        table: 'tasks',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    }
  });

  return db.createTable('tasks', {
    id: { type: 'bigint',
      primaryKey: true
    },
    name: {type:'string', length: 100}
  });

};

exports.down = function(db) {
  db.dropTable('workouts');
  db.dropTable('workouts_tasks');
  return db.dropTable('tasks');
};

exports._meta = {
  "version": 1
};
