'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.addColumn('trainers', 'disabled', {type:'boolean'}, err => err && console.info(`Error: ${err}`));

  return db.createTable('trainers_clients', {
    trainer_id: { type: 'bigint',
      primaryKey: true,
      foreignKey: {
        name: 'c_trainers_id_fk',
        table: 'trainers',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'user'
      }
    },
    client_id: { type: 'bigint',
      primaryKey: true,
      foreignKey: {
        name: 'kind_sport_id_fk',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    },
  });
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
