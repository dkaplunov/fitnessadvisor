'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.runSql(
      "CREATE SEQUENCE public.workouts_seq INCREMENT 1 START 100;" +
      "ALTER TABLE public.workouts ALTER COLUMN id SET DEFAULT nextval('workouts_seq'::regclass);" +
      " ALTER TABLE public.users ALTER COLUMN id SET DEFAULT nextval('workouts_seq'::regclass);" +
      " ALTER TABLE public.workouts_tasks ALTER COLUMN id SET DEFAULT nextval('workouts_seq'::regclass);" +
      " ALTER TABLE public.tasks ALTER COLUMN id SET DEFAULT nextval('workouts_seq'::regclass);",
      [], (err)=>{console.info(err)});
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};

