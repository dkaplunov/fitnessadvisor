'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  db.createTable('sport_kinds', {
    id: { type: 'bigint',
      primaryKey: true
    },
    name: {type:'string', length: 100},
  });


  return db.createTable('trainers', {
    reg_date: {type:'date'},
    qualification: {type:'string', length: 100},
    rank: {type:'decimal', length: 4},
    user: {
      type: 'bigint',
      primaryKey: true,
      foreignKey: {
        name: 'users_id_fk',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    }
  });
};


exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
