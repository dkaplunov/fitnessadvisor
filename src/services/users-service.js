'use strict';

const userAdapter = require('../data/users_adapter');
const trainerAdapter = require('../data/trainers_adapter');
const messages = require('../resources/constants').constants;
const bcrypt = require('bcrypt');
const logger = require('../services/utils').logger();

module.exports.signIn = (signInInfo, testAdapter) => {
    return new Promise((resolve, reject) =>
        (testAdapter||trainerAdapter).findUserByName(signInInfo).then (data => {
        bcrypt.compare(signInInfo.password, data.password).then((res) => {
            if (res) {
                (testAdapter||trainerAdapter).getTrainerInfo(data.id).then(trInfo => resolve({mess:messages.SUCCESS_AUTHENTICATION, user:{...data, password:'', isTrainer:true}})).catch(
                    (res) => {
                        if (!res) {
                            resolve({mess:messages.SUCCESS_AUTHENTICATION, user:{...data, password:''}});
                        } else {
                            reject (messages.AUTHENTICATION_ERROR);
                        }
                    }
                );
            } else {
                logger.error(messages.AUTHENTICATION_ERROR.text);
                reject (messages.AUTHENTICATION_ERROR);
            }
        }).catch((res) => {
            logger.error(res);
            reject (messages.GENERAL_ERROR);
        });
    }).catch((res)=> {
        logger.error(messages.AUTHENTICATION_ERROR.text+': '+res);
        reject (messages.AUTHENTICATION_ERROR);
    }))

};

module.exports.update = (updateInfo, testAdapter) => {
  return saveUserInfo(updateInfo, (testAdapter||userAdapter).updateUser, testAdapter);
};

module.exports.signUp = (signUpInfo, testAdapter, update) => {
    return saveUserInfo(signUpInfo, (testAdapter||userAdapter).createUser, testAdapter)
};

module.exports.getClients = (userId, testAdapter, update) => {
    return (testAdapter||trainerAdapter).getClients(userId);
};

module.exports.getTrainers = (testAdapter) => {
    return (testAdapter||trainerAdapter).getTrainers();
};

module.exports.getTrainersForUser = (testAdapter) => {
    return (testAdapter||trainerAdapter).getTrainersForUser();
};

module.exports.setClient = (userId, trainerId, testAdapter) => {
    return (testAdapter||trainerAdapter).setClient(userId, trainerId);
};

module.exports.unSetClient = (userId, trainerId, testAdapter) => {
    return (testAdapter||trainerAdapter).unSetClient(userId, trainerId);
};

const saveUserInfo = (userInfo, savingMethod, testAdapter) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(userInfo.password || '', 10).then( hash =>{
            userInfo.password = userInfo.password && hash;
            savingMethod(userInfo).then (data => {
                (testAdapter||trainerAdapter).updateTrainerInfo(data.id, userInfo.isTrainer).then(
                    () => resolve({message:messages.SUCCESS_SAVED, result:{...data, isTrainer:userInfo.isTrainer}}))
                    .catch(e => {
                            logger.error(e);
                            reject(e)
                    })
            }).catch((res) => {
                logger.error(res);
                reject (messages.ERROR_SAVING);
            })
        })
    });
};
