'use strict';

const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const messages = require('../resources/constants').constants;

module.exports.isAuthenticated = (req, res, next) => {
    if (req.session.user) {
        next();
    } else {
        res.status(401).send({message:'Authentication needed!'});
    }
};

const logger = () => {
    return winston.createLogger({
        level: process.env.NODE_ENV&&process.env.NODE_ENV==='dev'?'debug':'error',
        format: winston.format.simple(),
        transports: [
            new winston.transports.Console(),
            new DailyRotateFile ({ filename: process.env.LOG_FILE_PATH||'./fitnessadvisor_site_log.log' })
        ]
    });
};

module.exports.getErrMessage = (err, step) => {
    logger().error(''+step+' --> '+err);
    return ({
        error: err,
        message: messages.ERROR_SAVING
    });
};

module.exports.logger = logger;