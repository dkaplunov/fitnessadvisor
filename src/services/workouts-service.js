'use strict';

const workoutAdapter = require('../data/workout_adapter');
const messages = require('../resources/constants').constants;
const getErrMessage = require('./utils').getErrMessage;
const logger = require('../services/utils').logger();

const addTasks = (tasks, workoutId) => {
    const promises = [];
    tasks.forEach(item => {
        promises.push(workoutAdapter.workoutTask.create({date:item.date, description:item.description, amount:item.amount, workout:workoutId, taskId:item.task.id}))
    });

    return Promise.all(promises);
};


module.exports.saveWorkout = (workoutData) => {
    return new Promise((resolve, reject) => {
        workoutAdapter.workout.findByPk(workoutData.id).then(result => {
            result.date = workoutData.date || new Date();
            result.user = workoutData.user.id;
            result.save().then(
                ()=>addTasks(workoutData.tasks, workoutData.id).then(()=>resolve(messages.SUCCESS_SAVED)).catch(err=>reject(getErrMessage(err, 1)))
            ).catch(err => reject(getErrMessage(err, 3)));
        }).catch(() => {
            workoutAdapter.workout.create({date:workoutData.date, user:workoutData.user}).then(
                (wrc)=>addTasks(workoutData.tasks, wrc.id).then(()=>resolve(messages.SUCCESS_SAVED)).catch(err=>reject(getErrMessage(err, 4)))
            ).catch(err => reject(getErrMessage(err, 5)));
        })
    });
};

module.exports.getWorkoutsForUser = (userId, date) => {
    return new Promise((resolve, reject) => {
        workoutAdapter.findWorkoutByUserAndDate(userId, date)
        .then (res => {
            resolve (res.map(wrc => ({
                id:wrc.dataValues.id,
                date:wrc.dataValues.date,
                user:wrc.dataValues.user,
                tasks:wrc.dataValues.workouts_tasks.map(item => ({
                    name:item.dataValues.task.dataValues.name,
                    id:item.dataValues.id,
                    date:item.dataValues.date,
                    amount:item.dataValues.amount})
                )
            })))
        }).catch(err => reject(err));
})};

module.exports.getTaskList = () => {
    return new Promise((resolve, reject) => {
        workoutAdapter.task.findAll().then (res => {
            resolve (res.map(item => ({
                name:item.dataValues.name,
                id:item.dataValues.id})
            ))})
        }).catch(err => reject(err));
};

module.exports.deleteTask = (taskId) => {
    return new Promise((resolve, reject) => {
        workoutAdapter.workoutTask.findByPk(taskId).then(task => task.destroy().then(()=>resolve()).catch(()=>reject())).catch(()=>reject());
    })
};
