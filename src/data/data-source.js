'use strict';

const connectionString = require('../resources/constants').CONNECTION_STRING;

const pgp = require('pg-promise')(/* options */);
const db = pgp(connectionString);

module.exports = db;