'use strict';

const logger = require('../services/utils').logger();
const Sequelize = require('sequelize');
const db = require('../data/seq_data_source');
const user = require('./users_adapter').user;
const mapUserInfo = require('./users_adapter').mapUserInfo;
const messages = require('../resources/constants').constants;
const qualificationLevels  = require('../resources/constants').qualificationLevels;
const getErrMessage = require('../services/utils').getErrMessage;


const trainer = db.define('trainers', {
    user: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    regDate: {
        type: Sequelize.DATE,
        field: 'reg_date'
    },
    qualification: {
        type: Sequelize.STRING
    },
    rank: {
        type:Sequelize.DECIMAL
    },
    disabled: {
        type:Sequelize.BOOLEAN
    },
}, {timestamps: false});

const trainerClients = db.define('trainers_clients', {
    trainerId: {
        type: Sequelize.BIGINT,
        field: 'trainer_id'
    },
    userId: {
        type: Sequelize.BIGINT,
        field: 'client_id'
    },
}, {timestamps: false});

const trainersSportKind = db.define('trainers_sport_kind', {
    trainerId: {
        type: Sequelize.BIGINT,
        field: 'trainer_id'
    },
    kindId: {
        type: Sequelize.BIGINT,
        field: 'kind_id'
    },
}, {timestamps: false});

const sportKind = db.define('sport_kinds', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
}, {timestamps: false});

const trainerUser = trainer.hasOne(user, {as: 'userInfo', foreignKey: 'id' , sourceKey: 'user'});
//user.hasOne(trainer, {as: 'uTrainer', foreignKey: 'user' , sourceKey: 'id'});
const trainerKinds = trainer.belongsToMany(sportKind, {
    as: 'sportKinds',
    through: trainersSportKind,
    foreignKey: 'trainerId',
});

sportKind.belongsToMany(trainer, {
    as: 'trainers',
    through: trainersSportKind,
    foreignKey: 'kindId',
});

const clients = trainer.belongsToMany(user, {
    as: 'clients',
    foreignKey: 'trainerId',
    through: trainerClients,
});

const trainers = user.belongsToMany(trainer, {
    as: 'trainers',
    foreignKey: 'userId',
    through: trainerClients,
});

const findUserByName = (userData) => {
    return new Promise((resolve, reject) => user.findOne({
            attributes:['id', 'name', 'height', 'weight', 'age', 'firstName', 'lastName', 'password'],
            where:userData.name&&{name:userData.name}||{id:userData.id},
            include: [{ association: trainers}]
        }).then(res=>{
            resolve (mapUserInfo(res))
        }).catch(e=>reject (e))
    );
};

module.exports.findUserByName = findUserByName;

module.exports.updateTrainerInfo = (userId, isTrainer) => {
    return new Promise((resolve, reject) => {
        trainer.findByPk(userId).then(trInfo => {
            if (trInfo) {
                trInfo.disabled = !isTrainer;
                trInfo.save().then(()=>resolve(messages.SUCCESS_SAVED)).catch((err)=>reject(getErrMessage(err, 3)));
            } else if (!trInfo && isTrainer) {
                trainer.create(
                    {user:userId, regDate:new Date(), qualification:qualificationLevels.BEGINNER.name, rank:qualificationLevels.BEGINNER.rank, disabled:false})
                    .then(()=>resolve(messages.SUCCESS_SAVED))
                    .catch((err)=>reject(getErrMessage(err, 3)));
            }
            resolve(messages.SUCCESS_SAVED);
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.getTrainerInfo = (userId) => {
    return new Promise((resolve, reject) => {
        trainer.findByPk(userId).then(trInfo => {
            if (trInfo && !trInfo.dataValues.disabled) { resolve(trInfo); }
            reject();
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.getTrainers = () => {
    return new Promise((resolve, reject) => {
        trainer.findAll({include: [{ association: trainerUser, attributes:['lastName', 'firstName']}, {association: trainerKinds}]}).then(trInfo => {
            if (trInfo) { resolve(trInfo); }
            reject();
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.getTrainersForUser = (userId) => {
    return new Promise((resolve, reject) => {
        trainer.findAll({include: [{ association: clients, where:{id:userId}},
                { association: trainerUser, attributes:['lastName', 'firstName']}]}).then(trInfo => {
            if (trInfo) { resolve(trInfo); }
            reject();
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.getClients = (userId) => {
    return new Promise((resolve, reject) => {
        trainer.findByPk(userId).then(trInfo => {
            resolve(trInfo?trInfo.getClients(
                {attributes:['id', 'name', 'lastName', 'firstName', 'age', 'weight', 'height']}):[]);
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.setClient = (userId, trainerId) => {
    return new Promise((resolve, reject) => {
        trainer.findByPk(trainerId).then(trInfo => {
            trInfo && trInfo.addClient(userId).then(()=>
                findUserByName({id:userId}).then(res=>resolve({...res, password:''})).catch(e => reject(e))
            ).catch(e=>reject(getErrMessage(e, 3)));
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.unSetClient = (userId, trainerId) => {
    return new Promise((resolve, reject) => {
        trainerClients.findOne({where:{trainerId:trainerId, userId:userId}}).then(trInfo => {
            trInfo && trInfo.destroy().then(()=>
                findUserByName({id:userId}).then(res=>resolve({...res, password:''})).catch(e => reject(e))
            ).catch(e=>getErrMessage(e, 3));
        }).catch((err) => {
            reject (getErrMessage(err, 3));
        });
    });

};

module.exports.trainer = trainer;

