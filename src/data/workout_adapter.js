'use strict';

const logger = require('../services/utils').logger();
const Sequelize = require('sequelize');
const user = require('./users_adapter').user;
const db = require('../data/seq_data_source');

const workout = db.define('workouts', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    date: {
        type: Sequelize.DATEONLY
    },
    user: {
        type: Sequelize.BIGINT,
    },
}, {timestamps: false});


const task = db.define('tasks', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {timestamps: false});

const workoutTask = db.define('workouts_tasks', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    date: {
        type: Sequelize.DATE
    },
    description: {
        type: Sequelize.STRING
    },
    amount: {
        type: Sequelize.INTEGER
    },
    taskId: {
        type: Sequelize.BIGINT,
    },
}, {timestamps: false});

workoutTask.hasOne(task, {as: 'task', foreignKey: 'id' , sourceKey: 'taskId'});
user.hasMany(workout, {foreignKey: 'user', sourceKey: 'id'});
workout.hasMany(workoutTask, {foreignKey: 'workout', sourceKey: 'id'});

module.exports.findWorkoutByUserId = (userId) => {
    return user.findOne({
        attributes:['id', 'name'],
        where:{id:userId},
        include: [{ all: true, nested: true }]
    })};

module.exports.findWorkoutByUserAndDate = (userId, date) => {
    let wDate = date || new Date();
    return workout.findAll({
        where:{user:userId, date:wDate},
        include: [{ all: true, nested: true }]
    })};

module.exports.workout = workout;
module.exports.user = user;
module.exports.workoutTask = workoutTask;
module.exports.task = task;

