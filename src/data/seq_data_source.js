const Sequelize = require('sequelize');
const connectionString = require('../resources/constants').CONNECTION_STRING;

const db = connectionString !== 'undefined' ? new Sequelize(connectionString, {}) : {define:()=>{return {hasOne:()=>{},belongsToMany:()=>{}}}}; //TODO "mocking" object for tests, must be refactored!!!

module.exports = db;