'use strict';

const logger = require('../services/utils').logger();
const Sequelize = require('sequelize');
const db = require('../data/seq_data_source');
const getErrMessage = require('../services/utils').getErrMessage;
const messages = require('../resources/constants').constants;
//const trainers = require('./trainers_adapter').trainers;

const user = db.define('users', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    height: {
        type:Sequelize.DECIMAL
    },
    weight: {
        type:Sequelize.DECIMAL
    },
    age: {
        type:Sequelize.INTEGER
    },
    firstName: {
        type:Sequelize.STRING,
        field:'first_name'
    },
    lastName: {
        type:Sequelize.STRING,
        field:'last_name'
    }

}, {timestamps: false});

module.exports.createUser = (userData) => {
    return new Promise((resolve, reject) => {
        user.findOne({attributes:['id', 'name', 'password'], where:{name:userData.name}}).then(result => {
            if (result) {
                reject(`User ${userData.name} already exists!`);
            } else {
                user.create(userData).then((us) => resolve({...us, id:us.dataValues.id}))
                    .catch(err => reject(getErrMessage(err, 5)));
            }
        }).catch(e=>reject (e))
    });
};

module.exports.updateUser = (userData) => {
    return new Promise((resolve, reject) => {
        user.findOne({attributes:['id'], where:{name:userData.name},}).then(result => {
            if (result) {
                result.name = userData.name;
                result.password = userData.password ? userData.password: undefined;
                result.weight = userData.weight;
                result.height = userData.height;
                result.age = userData.age;
                result.firstName = userData.firstName;
                result.lastName = userData.lastName;
                result.save().then(
                    (us) => resolve (mapUserInfo(us))
                ).catch(err => reject(getErrMessage(err, 3)));
            } else {
                reject(`User ${userData.name} doesn't exist!`);
            }
        }).catch(e=>reject (e))
    });

};

const mapUserInfo = (dbUser) => {
    return {
        id:dbUser.dataValues.id,
        name:dbUser.dataValues.name,
        firstName:dbUser.dataValues.firstName,
        lastName:dbUser.dataValues.lastName,
        weight:dbUser.dataValues.weight,
        height:dbUser.dataValues.height,
        age:dbUser.dataValues.age,
        password:dbUser.password,
        trainers:dbUser.dataValues.trainers && dbUser.dataValues.trainers.map(item=>{return {trainerId:item.dataValues.user}}) || []

    }
};

module.exports.user = user;
module.exports.mapUserInfo = mapUserInfo;