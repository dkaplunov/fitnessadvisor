'use strict';

const express = require('express');

const router = express.Router();

const userService = require ('../services/users-service');
const messages = require('../resources/constants').constants;
const logger = require('../services/utils').logger();
const utils = require('../services/utils');


router.post('/signIn', (req, res, next)=> {
    logger.info('signIn');
    userService.signIn(req.body).then (result => {
        req.session.user={user:result.user};
        res.status(result.mess.code).send ({result:result});
    }).catch(mess => res.status(mess.code).send (mess));
});

router.get('/signOut', (req, res, next)=> {
    const user = req.session.user;
    req.session.user=null;
    logger.info('signOut');
    res.status(messages.SUCCESS_LOGOUT.code).send ({message:messages.SUCCESS_LOGOUT, result:user});
});

router.get('/userStatus', (req, res, next)=> {
    logger.info('getStatus');
    res.status(
        req.session.user?messages.SUCCESS_AUTHENTICATION.code:messages.AUTHENTICATION_ERROR.code
    ).send ({result:req.session.user});
});

router.post('/signUp', (req, res, next)=> {
    logger.info('signUp');
    userService.signUp(req.body).then (mess => {
        res.status(mess.code).send (mess)
    }).catch(mess => res.status(mess.code).send (mess));
});

router.post('/update', utils.isAuthenticated, (req, res, next)=> {
    logger.info('update user info');
    userService.update(req.body).then (result => {
        res.status(result.message.code).send (result)
    }).catch(mess => res.status(mess.code||500).send (mess));
});

router.get('/clientsForTrainer/:userId', utils.isAuthenticated, (req, res, next)=> {
    logger.info('get clients for trainer');
    userService.getClients(req.params.userId).then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_FOUND, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.get('/trainersList', utils.isAuthenticated, (req, res, next)=> {
    logger.info('trainers list');
    userService.getTrainers().then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_FOUND, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.get('/trainersForUser/:userId', utils.isAuthenticated, (req, res, next)=> {
    logger.info('trainers list');
    userService.getTrainersForUser(req.params.userId).then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_FOUND, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.get('/registerForTrainer/:trainerId', utils.isAuthenticated, (req, res, next)=> {
    logger.info('register client for trainer');
    userService.setClient(req.session.user.user.id, req.params.trainerId).then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_SAVED, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.get('/unRegisterClient/:trainerId', utils.isAuthenticated, (req, res, next)=> {
    logger.info('Unregister client for trainer');
    userService.unSetClient(req.session.user.user.id, req.params.trainerId).then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_SAVED, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});


module.exports = router;
