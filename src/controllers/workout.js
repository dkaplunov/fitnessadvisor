'use strict';

const express = require('express');

const router = express.Router();
const messages = require('../resources/constants').constants;
const logger = require('../services/utils').logger();
const utils = require('../services/utils');

const workoutService = require('../services/workouts-service');

router['get']('/forUser/:userId/:date', utils.isAuthenticated, (req, res, next)=> {
    logger.info('get workout for user');
    workoutService.getWorkoutsForUser(req.params.userId, new Date(req.params.date)).then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_FOUND, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.post('/save', utils.isAuthenticated, (req, res, next)=> {
    logger.info('save workout');
    workoutService.saveWorkout((req.body)).then (result => {
        res.status(result.code).send (result);
    }).catch(mess => res.status(mess.message.code||500).send (mess));
});

router.get('/tasksList', utils.isAuthenticated, (req, res, next)=> {
    logger.info('get tasks list');
    workoutService.getTaskList().then (result => {
        res.status(messages.SUCCESS_FOUND.code).send ({message:messages.SUCCESS_FOUND, result:result})
    }).catch(
        result => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND, result: result.message});
        }
    );
});

router.get('/deleteTask/:taskId', utils.isAuthenticated, (req, res, next)=> {
    logger.info(`delete task ${req.params.taskId}`);
    workoutService.deleteTask(req.params.taskId).then (() => {
        res.status(messages.SUCCESS_SAVED.code).send ({message:messages.SUCCESS_SAVED})
    }).catch(
        () => {
            res.status(messages.NOT_FOUND.code).send ({message:messages.NOT_FOUND});
        }
    );
});



module.exports = router;
