'use strict';

module.exports.constants = {
    SUCCESS_FOUND:{state:'SUCCESS FOUND', text:'Object successfully found', code:200},
    SUCCESS_SAVED:{state:'SUCCESS SAVED', text:'Object successfully saved', code:200},
    SUCCESS_LOGOUT:{state:'SUCCESS LOGOUT', text:'User successfully logout', code:200},
    SUCCESS_AUTHENTICATION:{state:'SUCCESS AUTHENTICATION', text:'User successfully authenticated', code:200},
    NOT_FOUND:{state:'NOT FOUND', text:'Object not found', code:404},
    ERROR_SAVING:{state:'ERROR SAVING', text:'Error while object saving', code:500},
    GENERAL_ERROR:{state:'GENERAL ERROR', text:'Something goes wrong', code:500},
    AUTHENTICATION_ERROR:{state:'AUTHENTICATION ERROR', text:'User name or password is not right', code:401},
    TEST_FAILED:'Test failed'
};

module.exports.qualificationLevels = {
    BEGINNER:{name:'Beginner', rank:0.5},
    PROFESSIONAL:{name:'Professional', rank:1},
    MASTER:{name:'Master', rank:2},
};

module.exports.CONNECTION_STRING = `${process.env.DATABASE_URL}`
//`postgres://${process..env.DB_USER}:${process..env.DB_PASSWORD}@${process..env.DB_HOST}:${process..env.DB_PORT||5432}/${process..env.DB_NAME}`;

