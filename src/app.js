'use strict';
const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const utils = require('./services/utils');
const logger = require('./services/utils').logger();
const cors = require('cors');
const app = express();
const port = process.env.SRV_PORT;
const corsCredential = process.env.CORS_CREDENTIAL;
const DBMigrate = require('db-migrate');

const users = require('./controllers/users');
const workouts = require('./controllers/workout');

const dbmigrate = DBMigrate.getInstance(true);
dbmigrate.up();

app.use(cors({credentials:true, origin:`${corsCredential}`}));

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

app.use(session({
    secret: 'fitnessadviser2019',
    resave: false,
    saveUninitialized: false
}));

app.get('/', utils.isAuthenticated,(req, res) => res.send({greetings:'Hello World!'}));
app.get('/hc',(req, res) => res.status(200).send({greetings:'Service is Ok!'}));

app.use('/user', users);
app.use('/workouts', workouts);

const swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('../api-doc/swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
const router = express.Router();
app.use('/api/v1', router);

app.listen(port, () => logger.info(`Example app listening on port ${port}!`));