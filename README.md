# FitnessAdvisor
To dev start type yarn start

To build docker image type: "docker build -t fitnessadvisor -f app.dockerfile ."
 
To to start prepared docker image type: docker run --env-file .env -p 4000:3000 -d fitnessadvisor 

To add migration type: node node_modules/db-migrate/bin/db-migrate create add-<migration name>

To send correct json message use request header  Content-Type=application/json

To upload image to AWS ECR:

    - install aws cli
    - run "aws configure" and set credentional - AWSAccessKeyId and AWSSecretKey
    - run "aws ecr get-login --no-include-email --region us-east-2"
    - run command docker -login ...
    - build docker image
    - run command "docker tag fitnessadvisor:latest <here URL from aws>"
    - run command  "docker push <here URL from aws>"
    
Environment variables (to set it in development mode use .env file or Idea run configuration setting): 

    -  NODE_ENV
    -  DATABASE_URL=postgres://postgres:<PASSWORD>@localhost:5432/fitnessadvisor
    -  LOG_FILE_PATH
    -  SRV_PORT
    -  CORS_CREDENTIAL - list of credentials URLs